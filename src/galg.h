#ifndef __GALG_H__
#define __GALG_H__

#include "Graph.h"
#include "PairingHeap.h"

#include <cstddef>
#include <deque>
#include <functional>
#include <iterator>
#include <stdexcept>

namespace galg
{
template <typename Iterator> class Vertex
{
  public:
    Vertex() = default;
    Vertex(Iterator it) noexcept : it(it) {}
    /* Vertex(const Vertex&) = delete;
    Vertex(Vertex&&)      = default; */

    /* Vertex& operator=(const Vertex&) = delete;
    Vertex& operator=(Vertex&&) = default; */

    size_t depth() const noexcept { return _depth; }

    Vertex& update(const Vertex& other) noexcept
    {
        _depth = other._depth + 1;
        return *this;
    }

    Iterator iter() const noexcept { return it; }

  private:
    Iterator it;
    size_t _depth = 0;
};

template <typename Iterator> Vertex<Iterator> vertex(Iterator it)
{
    return Vertex<Iterator>{it};
}

template <typename Iterator,
          typename Cost,
          typename Compare = std::less<Cost>,
          typename Combine = std::plus<Cost>>
class WeightedVertex : public Vertex<Iterator>
{
  public:
    WeightedVertex() = default;

    WeightedVertex(Iterator it, const Cost& empty) noexcept
        : Vertex<Iterator>(it), _cost(empty), _distance(empty)
    {
    }

    WeightedVertex(Iterator it,
                   const Cost& cost,
                   const WeightedVertex& other) noexcept
        : Vertex<Iterator>(it), _cost(cost)
    {
        Vertex<Iterator>::update(other);
        _distance = plus(other._distance, cost);
    }

    const Cost& cost() const noexcept { return _cost; }
    const Cost& distance() const noexcept { return _distance; }

    bool light(const Cost& empty) const noexcept
    {
        return less(_distance, empty);
    }

    WeightedVertex& update(const Cost& cost,
                           const WeightedVertex& other) noexcept
    {
        if (less(plus(other._distance, cost), _distance)) {
            Vertex<Iterator>::update(other);
            _cost     = cost;
            _distance = plus(other._distance, _cost);
        }

        return *this;
    }

    friend bool operator<(const WeightedVertex& lhs, const WeightedVertex& rhs)
    {
        Compare less;
        return less(lhs._distance, rhs._distance);
    }

  private:
    Cost _cost;
    Cost _distance;

    Compare less;
    Combine plus;
};

template <typename Iterator, typename Cost, typename... Scale, typename... Args>
WeightedVertex<Iterator, Cost, Scale...>
tare(Iterator it, const Cost& empty, Args&&... args)
{
    return WeightedVertex<Iterator, Cost, Scale...>{
        it, empty, std::forward<Args>(args)...};
}

template <typename Iterator, typename Cost, typename... Scale, typename... Args>
WeightedVertex<Iterator, Cost, Scale...>
weigh(Iterator it,
      const Cost& cost,
      const WeightedVertex<Iterator, Cost, Scale...>& other,
      Args&&... args)
{
    return WeightedVertex<Iterator, Cost, Scale...>{
        it, cost, other, std::forward<Args>(args)...};
}

/**
 * @brief Runs breadth first search on a graph, applying a given function to the
 * elements as they are visited.
 *
 * @tparam Iterator A graph iterator type (const or not).
 * @tparam Visitor A callable type that can be applied to elements as they are
 * visited in the search.
 * @tparam Args The type of any extra arguments needed to be passed as elements
 * are visited.
 * @param s A wrapped iterator representing the source element.
 * @param visit The callable to be applied to the elements.
 * @param args The extra arguments to be passed to visit.
 */
template <typename Iterator, typename Visitor, typename... Args>
void bfs(Vertex<Iterator> s, Visitor visit, Args&&... args)
{
    using Vertex       = Vertex<Iterator>;
    using Queue        = std::deque<Vertex>;
    using Neighborhood = Neighborhood<Iterator>;
    using Neighbor     = typename Neighborhood::reference;

    Queue queue;
    Neighborhood canvased;

    queue.insert(queue.end(), s);
    canvased.checkoff(s.iter());

    while (!queue.empty()) {
        const Vertex& u = queue.front();

        visit(u, std::forward<Args>(args)...);

        canvased = neighborhood(u.iter(), std::move(canvased));

        for (Neighbor n : canvased) {
            Iterator it = n.iter();

            if (!canvased.seen(it)) {
                queue.insert(queue.end(), vertex(it).update(u));
                canvased.checkoff(it);
            }
        }

        queue.pop_front();
    }
}

/**
 * @brief Runs Dijkstra's shortest-path algorithm on a graph, applying a given
 * function to the elements as they are visited.
 *
 * @tparam Iterator A graph iterator type (const or not).
 * @tparam Cost The type of the cost of edges in the graph.
 * @tparam Scale Types that are callable (binary functions) representing how to
 * compare and combine the Cost type
 * @tparam Visitor A callable type that can be applied to elements as they are
 * visited in the search.
 * @tparam Args The type of any extra arguments needed to be passed as elements
 * are visited.
 * @param s A wrapped iterator representing the source element.
 * @param visit The callable to be applied to the elements.
 * @param args The extra arguments to be passed to visit.
 */
template <typename Iterator,
          typename Cost,
          typename... Scale,
          typename Visitor,
          typename... Args>
void dijkstra(WeightedVertex<Iterator, Cost, Scale...> s,
              Visitor visit,
              Args&&... args)
{
    using Vertex = WeightedVertex<Iterator, Cost, Scale...>;

    using Queue    = PairingHeapQueueAdaptor<Vertex>;
    using Priority = typename Queue::iterator;

    using Neighborhood = Neighborhood<Iterator>;
    using Map          = std::unordered_map<size_t, Priority>;

    using Neighbor = typename Neighborhood::reference;

    Queue queue;
    Neighborhood canvased;
    Map map;

    const Cost& empty = s.cost();

    queue.insert(queue.end(), s);

    while (!queue.empty()) {
        const Vertex& u = queue.front();
        canvased.checkoff(u.iter());

        visit(u, std::forward<Args>(args)...);

        canvased = neighborhood(u.iter(), std::move(canvased));

        for (Neighbor n : canvased) {
            Iterator it = n.iter();

            if (!canvased.seen(it)) {
                size_t address = canvased.search(it);

                Vertex v;

                try {
                    Priority& priority         = map.at(address);
                    v                          = *priority;
                    queue.prioritize(priority) = v.update(n.cost(), u);
                } catch (const std::out_of_range& e) {
                    v                 = weigh(it, n.cost(), u);
                    Priority priority = queue.insert(queue.end(), v);
                    map[address]      = priority;
                }

                if (v.light(empty)) {
                    throw std::logic_error("galg::dijkstra");
                }
            }
        }

        queue.pop_front();
    }
}

/**
 * @brief Check if a graph is connected. A graph is connected if given any two
 * elements, a path (consisting of edges in the graph) can be found between
 * those elements.
 *
 * @tparam T The type of the elements the graph being checked contains.
 * @tparam Cost The type of the cost between elements in the graph being
 * checked.
 * @param g The graph to check.
 * @return True if the graph is connected, and false otherwise.
 */
template <typename T, typename Cost>
bool connected(const Graph<T, Cost>& g) noexcept
{
    // empty graph is disconnected by definition
    if (g.empty()) return false;

    size_t v   = 0;
    auto visit = [](auto&, size_t& v) { ++v; };
    bfs(vertex(g.cbegin()), visit, v);

    return v == g.size(); // did we traverse a spanning tree?
}

} // namespace galg

#endif
