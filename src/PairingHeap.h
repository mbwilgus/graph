#ifndef __PAIRING_HEAP_H__
#define __PAIRING_HEAP_H__

#include <algorithm>
#include <cstddef>
#include <deque>
#include <iterator>
#include <memory>
#include <stack>
#include <string>
#include <sys/types.h>
#include <utility> // for swap

#define _P_UNUSED_ __attribute__((unused))

// forward declare PairingHeap in order to forward declare swap for PairingHeap
template <typename T, typename Compare> class PairingHeap;

template <typename T, typename Compare>
void swap(PairingHeap<T, Compare>& first, PairingHeap<T, Compare>& second);

template <typename T, typename Compare = std::less<T>> class PairingHeap
{
  private:
    struct Node {
        Node() = default;
        Node(const T& data) : data(data) {}
        ~Node() = default;

        inline void addChild(Node* node)
        {
            node->parent = this;
            node->next   = left;
            left         = node;
        }

        T data;

        Node* left   = nullptr;
        Node* next   = nullptr;
        Node* parent = nullptr;
    };

    class Incrementer
    {
      public:
        Node* operator()(Node* node)
        {
            auto nop = [](_P_UNUSED_ Node* node) {};
            return operator()(node, nop);
        }

        template <typename Visitor> Node* operator()(Node* node, Visitor visit)
        {
            Node* next = nullptr;

            if (node->next) {
                if (node->left) {
                    queue.push_back(node->left);
                }
                next = node->next;
            } else if (node->left) {
                if (!queue.empty()) {
                    queue.push_back(node->left);
                    next = queue.front();
                    queue.pop_front();
                } else {
                    next = node->left;
                }
            } else if (!queue.empty()) {
                next = queue.front();
                queue.pop_front();
            }

            visit(node);

            return next;
        }

      private:
        std::deque<Node*> queue;
    };

    class NodeIterator
    {
        friend PairingHeap;

      public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = const T;
        using pointer           = const T*;
        using reference         = const T&;

        NodeIterator() = default;
        NodeIterator(Node* node) : node(node) {}
        NodeIterator(const NodeIterator& source)
            : node(source.node), increment(source.increment)
        {
        }

        reference operator*() const { return node->data; }
        pointer operator->() const { return std::addressof(node->data); }

        NodeIterator& operator++()
        {
            node = increment(node);
            return *this;
        }

        NodeIterator operator++(int)
        {
            NodeIterator tmp(*this);
            ++(*this);
            return tmp;
        }

        friend bool operator==(const NodeIterator& lhs, const NodeIterator& rhs)
        {
            return lhs.node == rhs.node;
        }

        friend bool operator!=(const NodeIterator& lhs, const NodeIterator& rhs)
        {
            return lhs.node != rhs.node;
        }

      private:
        Node* node;
        Incrementer increment;
    };

    /* required by standard (until c++17) to inherit type aliases this way, but
    is too ugly and also deprecated in more recent standards */
    class PriorityIterator /* : public std::iterator<std::output_iterator_tag,
                              void, void, void, void> */
    {
      public:
        using iterator_category = std::output_iterator_tag;
        using difference_type   = void;
        using value_type        = void;
        using pointer           = void;
        using reference         = void;

        PriorityIterator() = default;
        PriorityIterator(PairingHeap* heap, NodeIterator it)
            : heap(heap), it(it)
        {
        }

        PriorityIterator& operator=(const T& data)
        {
            heap->decrease(it, data);
            return *this; // NOTE: this is unused by definition
        }

        PriorityIterator& operator*() { return *this; }

        PriorityIterator& operator++()
        {
            ++it;
            return *this;
        }

        PriorityIterator operator++(int)
        {
            PriorityIterator tmp(*this);
            ++(*this);
            return tmp;
        }

      private:
        PairingHeap* heap;
        NodeIterator it;
    };

  public:
    using value_type      = T;
    using reference       = T&;
    using const_reference = const T&;

    using iterator       = NodeIterator;
    using const_iterator = NodeIterator;
    using priority       = PriorityIterator;

    PairingHeap() noexcept { init(); }

    PairingHeap(const PairingHeap& source) { copy(source); }

    PairingHeap(PairingHeap&& source) noexcept
        : root(source.root), _size(source._size)
    {
        source.init();
    }

    template <typename InputIterator>
    PairingHeap(InputIterator first, InputIterator last)
    {
        insert(first, last);
    }

    PairingHeap& operator=(const PairingHeap& source)
    {
        if (&source != this) {
            copy(source);
        }

        return *this;
    }

    PairingHeap& operator=(PairingHeap&& source)
    {
        if (&source != this) {
            root  = source.root;
            _size = source._size;
            source.init();
        }

        return *this;
    }

    ~PairingHeap()
    {
        walk(root, [](Node* node) { delete node; });
    }

    iterator insert(const_reference value)
    {
        Node* node = new Node{value};
        root       = merge(root, node);
        ++_size;

        return iterator{node};
    }

    iterator insert(_P_UNUSED_ const_iterator hint, const_reference value)
    {
        return insert(value);
    }

    template <typename InputIterator>
    void insert(InputIterator first, InputIterator last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }

        _size += std::distance(first, last);
    }

    iterator erase(const_iterator pos)
    {
        if (pos.node == root) {
            pop();
            return begin();
        } else {
            Node* node = pos.node;
            ++pos;
            cut(node);
            delete node;
            --_size;

            return pos;
        }
    }

    iterator erase(const_iterator first, const_iterator last)
    {
        for (; first != last;) {
            first = earse(first);
        }

        return first;
    }

    void pop()
    {
        Node* oldRoot = root;
        root          = pair(root->left);
        delete oldRoot;
        --_size;
    }

    void decrease(iterator pos, const_reference data)
    {
        Node* node = pos.node;
        if (Compare{}(data, node->data)) {
            node->data = data;
            if (node != root) {
                cut(node);
                root = merge(root, node);
            }
        }
    }

    size_t size() const { return _size; }
    bool empty() const { return begin() == end(); }

    const_reference top() const { return root->data; }

    iterator begin() { return iterator{root}; }
    const_iterator begin() const { return cbegin(); }
    const_iterator cbegin() const { return const_iterator{root}; }
    iterator end() { return iterator{nullptr}; }
    const_iterator end() const { return cend(); }
    const_iterator cend() const { return const_iterator{nullptr}; }

    priority prioritize(iterator pos) { return priority{this, pos}; }

  private:
    Node* root;
    size_t _size;

    inline void init()
    {
        root  = nullptr;
        _size = 0;
    }

    Node* merge(Node* a, Node* b)
    {
        /* NOTE: in the case that both a and b are null, this case will
           return */
        if (a == nullptr) {
            return b;
        }

        if (b == nullptr) {
            return a;
        }

        /* merge the two heaps where the result has a root that is the
           smaller of the original heaps */
        if (Compare{}(a->data, b->data)) {
            a->addChild(b);
            return a;
        } else {
            b->addChild(a);
            return b;
        }
    }

    Node* pair(Node* node)
    {
        if (node == nullptr) {
            return nullptr;
        }

        // combine siblings pairwise
        std::stack<Node*> subHeaps;
        while (node && node->next) {
            Node* next = node->next->next;
            subHeaps.push(merge(node, node->next));
            node = next;
        }

        // initialize second pass
        Node* newRoot;
        if (node != nullptr) {
            newRoot = node;
        } else {
            newRoot = subHeaps.top();
            subHeaps.pop();
        }

        // merge subheaps until we have a complete tree
        while (!subHeaps.empty()) {
            newRoot = merge(newRoot, subHeaps.top());
            subHeaps.pop();
        }

        // reset the parent of the winner
        newRoot->parent = nullptr;

        return newRoot;
    }

    void cut(Node* node)
    {
        // the node being cut is the leftmost child of it's parent
        if (node == node->parent->left) {
            node->parent->left = node->next;

            // find where the node is situated as a child and relink
        } else {
            Node* begin = node->parent->left;
            while (begin->next != node) {
                begin = begin->next;
            }
            begin->next = node->next;
        }

        // the node is now the root of it's own heap
        node->parent = nullptr;
        node->next   = nullptr;
    }

    template <typename Visit> void walk(Node* node, Visit visit)
    {
        Incrementer increment;

        while (node) {
            node = increment(node, visit);
        }
    }

    void copy(const PairingHeap& source)
    {
        root           = new Node{*source.root};
        Node* bookmark = root;

        Incrementer increment;

        auto copyChildren = [&bookmark, &increment](const Node* const node) {
            if (node->left) {
                Node* cursor = bookmark;
                cursor->left = new Node{*node->left};
                cursor       = cursor->left;
                Node* next   = node->left->next;
                while (next) {
                    cursor->next = new Node{*next};
                    cursor       = cursor->next;
                    next         = next->next;
                }
            }

            increment(bookmark);
        };

        walk(source.root, copyChildren);
        _size = source._size;
    }

    friend void swap(PairingHeap& first, PairingHeap& second)
    {
        using std::swap;

        swap(first.root, second.root);
        swap(first._size, second._size);
    }
};

// TODO: wrap all PairingHeap methods
template <typename T, typename Compare = std::less<T>>
class PairingHeapQueueAdaptor
{
  public:
    using Heap           = PairingHeap<T, Compare>;
    using iterator       = typename Heap::iterator;
    using const_iterator = typename Heap::const_iterator;
    using priority       = typename Heap::priority;

    inline iterator insert(_P_UNUSED_ const_iterator hint, const T& data)
    {
        return heap.insert(hint, data);
    }

    inline void pop_front() { heap.pop(); }

    inline size_t empty() const { return heap.empty(); }

    inline const T& front() const { return heap.top(); }

    inline iterator begin() { return heap.begin(); }
    inline iterator end() { return heap.end(); }
    inline priority prioritize(iterator pos) { return heap.prioritize(pos); }

  private:
    Heap heap;
};

#undef _P_UNUSED_
#endif
