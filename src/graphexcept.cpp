#include "graphexcept.h"

#include <stdexcept>
#include <string>

mismatched_owner::mismatched_owner(const std::string& what_arg)
    : std::invalid_argument(what_arg)
{
}

mismatched_owner::mismatched_owner(const char* what_arg)
    : std::invalid_argument(what_arg)
{
}

mismatched_owner::mismatched_owner(const mismatched_owner& source) noexcept
    : std::invalid_argument(source)
{
}

mismatched_owner&
mismatched_owner::operator=(const mismatched_owner& rhs) noexcept
{
    if (&rhs != this) std::logic_error::operator=(rhs);

    return *this;
}

adjacency_error::adjacency_error(const std::string& what_arg)
    : std::domain_error(what_arg)
{
}

adjacency_error::adjacency_error(const char* what_arg)
    : std::domain_error(what_arg)
{
}

adjacency_error::adjacency_error(const adjacency_error& source) noexcept
    : std::domain_error(source)
{
}

adjacency_error&
adjacency_error::operator=(const adjacency_error& rhs) noexcept
{
    if (&rhs != this) std::domain_error::operator=(rhs);

    return *this;
}
