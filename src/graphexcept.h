#ifndef __graph_except_h__
#define __graph_except_h__

#include <stdexcept>
#include <string>

class mismatched_owner : public std::invalid_argument
{
  public:
    mismatched_owner(const std::string& what_arg);
    mismatched_owner(const char* what_arg);
    mismatched_owner(const mismatched_owner& source) noexcept;
    mismatched_owner& operator=(const mismatched_owner& rhs) noexcept;
};

class adjacency_error : public std::domain_error
{
  public:
    adjacency_error(const std::string& what_arg);
    adjacency_error(const char* what_arg);
    adjacency_error(const adjacency_error& source) noexcept;
    adjacency_error& operator=(const adjacency_error& rhs) noexcept;
};

#endif
