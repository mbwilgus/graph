#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "graphexcept.h"
#include "graphextern.h"

#include <cmath>
#include <cstddef>
#include <functional>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>

/**
 * @brief An undirected graph container class. Elements are stored and
 * relationships among the elements are encoded by the notion of edges between
 * them. Edges have associated costs. Elements that are related by an edge are
 * called adjacent.
 *
 * @tparam T The type of the elements.
 * @tparam Cost The type of the cost of elements that are adjacent.
 */
template <typename T, typename Cost = double> class Graph
{

  private:
    struct Vertex;

    class VertexIterator;
    class ConstVertexIterator;

  public:
    using value_type      = T;
    using cost_type       = Cost;
    using reference       = value_type&;
    using const_reference = const value_type&;
    // using pointer =
    // using const_pointer =
    using iterator       = VertexIterator;
    using const_iterator = ConstVertexIterator;
    // using reverse_iterator =
    // using const_reverse_iterator =

  private:
    struct CostPtr {
        CostPtr() = delete;
        CostPtr(const Cost& cost) noexcept : cost(new Cost{cost}) {}
        CostPtr(const CostPtr& source) noexcept
            : cost(source.cost), count(source.count)
        {
            ++*count;
        }

        ~CostPtr()
        {
            if (--*count == 0) {
                delete cost;
                delete count;
            }
        }

        operator Cost&() { return *cost; }
        operator const Cost&() const { return *cost; }

        Cost* cost;
        unsigned short* count = new unsigned short{1};
    };

    class Address
    {
      public:
        Address(Vertex* v, const CostPtr& cost) noexcept : _v(v), _cost(cost) {}

        iterator iter() noexcept { return iterator{_v}; }
        const_iterator iter() const noexcept { return const_iterator{_v}; }

        Cost& cost() noexcept { return _cost; }
        const Cost& cost() const noexcept { return _cost; }

        inline Vertex* v() noexcept { return _v; }
        inline const Vertex* v() const noexcept { return _v; }

      private:
        Vertex* _v = nullptr;
        CostPtr _cost;
    };

    class Neighbor : public Address
    {
      public:
        Neighbor(Vertex* v, const CostPtr& cost) noexcept : Address(v, cost) {}
        Vertex* v() = delete;
    };

    struct Node {
        Node() = default;

        inline void link(Node* pos) noexcept
        {
            // Node* pos = this->prev;

            prev            = pos;
            next            = pos->next;
            pos->next->prev = this;
            pos->next       = this;
        }

        inline void unlink() noexcept
        {
            // NOTE: test if this is really needed
            // Node* next       = node->next;
            prev->next = next; // next;
            next->prev = prev;
        }

        Node* prev = this;
        Node* next = this;
    };

    struct AdjacencyNode : public Node {
        AdjacencyNode() = default;
        AdjacencyNode(AdjacencyNode* owner) noexcept : owner(owner) {}

        AdjacencyNode* owner = this;
    };

    struct AdjacencyList : AdjacencyNode {
        AdjacencyList() = default;

        AdjacencyList(const AdjacencyList& source)
            : size(source.size), edges(source.edges)
        {
            using Copies = std::unordered_map<const Vertex*, Vertex*>;

            Copies copies;

            // memoize copied vertices
            auto lookup = [this](Vertex* v, Copies& copies) {
                try {
                    return copies.at(v);
                } catch (const std::out_of_range& e) {
                    Vertex* copy = new Vertex{*v, this};
                    copies[v]    = copy;
                    return copy;
                }
            };

            Node* node = source.next;
            while (node != &source) {
                auto x    = static_cast<Vertex*>(node);
                Vertex* u = lookup(x, copies);

                u->link(this->prev);

                for (const Address& n : x->neighborhood) {
                    Vertex* v = lookup(n.v(), copies);
                    u->connect(v, n.cost());
                }

                node = node->next;
            }
        }

        ~AdjacencyList() { destroy(); }

        Vertex* insert(Node* pos, const T& data) noexcept
        {
            auto node = static_cast<AdjacencyNode*>(pos);
            check(node, "Graph::insert");

            Vertex* v = new Vertex{data, this};
            v->link(node);

            ++size;

            return v;
        }

        AdjacencyNode* erase(Node* node)
        {
            auto v = static_cast<Vertex*>(node);
            check(v, "Graph::erase");

            auto next = static_cast<AdjacencyNode*>(v->next);

            v->unlink();
            --size;
            edges -= v->isolate();

            delete v;

            return next;
        }

        void collocate(Node* n, Node* m, const Cost& cost)
        {
            auto u = static_cast<Vertex*>(n);
            auto v = static_cast<Vertex*>(m);
            check(u, v, "Graph::collocate");

            CostPtr cptr{cost};

            bool to   = u->connect(v, cptr);
            bool from = true;

            if (to && u != v) from = v->connect(u, cptr);
            if (to && from) ++edges;
        }

        void sever(Node* n, Node* m)
        {
            auto u = static_cast<Vertex*>(n);
            auto v = static_cast<Vertex*>(m);
            check(u, v, "Graph::sever");

            bool to   = u->disconnect(v);
            bool from = true;

            if (to && u != v) from = v->disconnect(u);
            if (to && from) --edges;
        }

        Cost cost(Node* n, Node* m) const
        {
            auto u = static_cast<Vertex*>(n);
            auto v = static_cast<Vertex*>(m);
            check(u, v, "Graph::cost");

            try {
                return u->cost(v);
            } catch (const std::out_of_range& e) {
                throw adjacency_error("Graph::cost");
            }
        }

        size_t degree(Node* node) const
        {
            auto v = static_cast<Vertex*>(node);
            check(v, "Graph::degree");

            return v->degree();
        }

        void splice(AdjacencyList&& other)
        {
            Node* node = other.next;
            while (node != &other) {
                node->owner = this;
                node        = node->next;
            }

            other.next->prev = this->prev;
            other.prev->next = this;
            this->prev->next = other.next;

            size += other.size;
            edges += other.edges;

            other.prev = other.next = &other;
            other.size = other.edges = 0;
        }

        AdjacencyNode* begin() noexcept
        {
            return static_cast<AdjacencyNode*>(this->next);
        }

        AdjacencyNode* end() noexcept { return this; }

        void clear()
        {
            destroy();

            this->next = this->prev = this;
            size = edges = 0;
        }

        size_t size  = 0;
        size_t edges = 0;

        void destroy()
        {
            Node* node = this->next;
            Node* next = node->next;

            while (node != this) {
                delete static_cast<Vertex*>(node);
                node = next;
                next = node->next;
            }
        }

        inline void check(AdjacencyNode* node, const char* msg) const
        {
            if (node->owner != this) throw mismatched_owner{msg};
        }

        inline void
        check(AdjacencyNode* n, AdjacencyNode* m, const char* msg) const
        {
            check(n, msg);
            check(m, msg);
        }
    };

    struct NeighborNode : public Node {
        NeighborNode(const Neighbor& neighbor,
                     const Vertex* key,
                     const size_t hash) noexcept
            : neighbor(neighbor), key(key), hash(hash)
        {
        }

        inline void prefer(const size_t capacity) noexcept
        {
            prefered = hash & (capacity - 1);
        }

        Neighbor neighbor;

        const Vertex* key;
        size_t hash;
        size_t prefered = 0;

        inline Neighbor* get() noexcept { return &neighbor; }
        inline const Neighbor* get() const noexcept { return &neighbor; }
    };

    struct Neighborhood {
        struct Storage : public Node {
            Storage(const size_t capacity)
            {
                size_t cap2 = 4;
                while (cap2 < capacity) {
                    cap2 <<= 1;
                }

                map = init(cap2);

                this->capacity = cap2;
            }

            ~Storage() { destroy(); }

            size_t search(const Vertex* key, const size_t hash) noexcept
            {
                for (size_t i = 0;; ++i) {
                    size_t index           = (hash + i) & (capacity - 1);
                    NeighborNode* neighbor = map[index];

                    bool found = neighbor == nullptr || neighbor->key == key;

                    if (found) {
                        return index;
                    }
                }
            }

            // neighbor is not null
            inline size_t search(NeighborNode* neighbor) noexcept
            {
                return search(neighbor->key, neighbor->hash);
            }

            void resize(size_t capacity)
            {
                NeighborNode** tmp = init(capacity);

                for (size_t i = 0; i < this->capacity; ++i) {
                    if (map[i] != nullptr) {
                        NeighborNode* neighbor = map[i];
                        neighbor->prefer(capacity);
                        for (size_t j = 0;; ++j) {
                            size_t index =
                                (neighbor->hash + j) & (capacity - 1);
                            if (tmp[index] == nullptr) {
                                tmp[index] = neighbor;
                                break;
                            }
                        }
                    }
                }

                this->capacity = capacity;

                delete[] map;
                map = tmp;
            }

            bool has(const size_t index) noexcept
            {
                return map[index] != nullptr;
            }

            NeighborNode* get(const size_t index) noexcept
            {
                return map[index];
            }

            void put(NeighborNode* neighbor, size_t index)
            {
                if (size + 1 > capacity * 0.75) {
                    resize(capacity << 1);
                    index = search(neighbor);
                }

                neighbor->prefer(capacity);

                map[index] = neighbor;
                ++size;

                neighbor->link(this->prev);
            }

            void rechain(size_t index) noexcept
            {
                size_t mod   = capacity - 1;
                size_t begin = (index + 1) & mod;

                for (size_t i = begin;; i = (i + 1) & mod) {
                    if (map[i] != nullptr) {
                        size_t prefered = map[i]->prefered;
                        if (prefered <= index ||
                            (i < begin && prefered >= index)) {
                            map[index] = map[i];
                            map[i]     = nullptr;
                            index      = i;
                        }
                    } else {
                        break;
                    }
                }
            }

            NeighborNode* take(const size_t index)
            {
                NeighborNode* neighbor = map[index];
                map[index]             = nullptr;
                neighbor->unlink();

                --size;

                if (size < capacity * 0.25) {
                    resize(capacity >> 1);
                } else {
                    rechain(index);
                }

                return neighbor;
            }

            NeighborNode** map;
            size_t capacity;
            size_t size = 0;

            inline static NeighborNode** init(const size_t capacity)
            {
                NeighborNode** map = new NeighborNode*[capacity];

                for (size_t i = 0; i < capacity; ++i) {
                    map[i] = nullptr;
                }

                return map;
            }

            inline void destroy()
            {
                for (size_t i = 0; i < capacity; ++i) {
                    delete map[i];
                }

                delete[] map;
            }
        };

        struct Canvaser {
            Canvaser(Node* node) noexcept : node(node) {}
            Canvaser(const Canvaser& source) noexcept : node(source.node) {}

            Canvaser& operator=(const Canvaser& source) noexcept
            {
                if (&source != this) {
                    node = source.node;
                }

                return *this;
            }

            Address& operator*() noexcept
            {
                return *static_cast<NeighborNode*>(node)->get();
            }

            Address* operator->() noexcept
            {
                return static_cast<NeighborNode*>(node)->get();
            }

            Canvaser& operator++() noexcept
            {
                node = node->next;
                return *this;
            }

            Canvaser operator++(int) noexcept
            {
                Canvaser tmp(*this);
                ++*this;
                return tmp;
            }

            friend bool operator==(const Canvaser& lhs,
                                   const Canvaser& rhs) noexcept
            {
                return lhs.node == rhs.node;
            }

            friend bool operator!=(const Canvaser& lhs,
                                   const Canvaser& rhs) noexcept
            {
                return lhs.node != rhs.node;
            }

            Node* node = nullptr;
        };

        Neighborhood() = default;

        ~Neighborhood() { delete storage; }

        bool connect(Vertex* v, const CostPtr& cost)
        {
            size_t hash  = h(v);
            size_t index = storage->search(v, hash);

            if (!storage->has(index)) {
                // TODO: use move semantics for Neighbor init
                NeighborNode* neighbor = new NeighborNode{{v, cost}, v, hash};
                storage->put(neighbor, index);

                return true;
            }

            return false;
        }

        bool disconnect(Vertex* v)
        {
            size_t hash  = h(v);
            size_t index = storage->search(v, hash);

            if (storage->has(index)) {
                NeighborNode* bucket = storage->take(index);
                delete bucket;

                return true;
            }

            return false;
        }

        void disassociate(Vertex* v) noexcept
        {
            for (Address& n : *this) {
                n.v()->disconnect(v);
            }
        }

        Cost cost(Vertex* v) const
        {
            size_t hash  = h(v);
            size_t index = storage->search(v, hash);

            if (storage->has(index)) {
                return storage->get(index)->cost;
            }

            throw std::out_of_range("Neighborhood::cost");
        }

        void clear()
        {
            delete storage;
            storage = new Storage(4);
        }

        size_t size() const noexcept { return storage->size; }

        Canvaser begin() noexcept { return Canvaser{storage->next}; }
        Canvaser end() noexcept { return Canvaser{storage}; }

        Storage* storage = new Storage(4);

        /* SEE:
         * https://stackoverflow.com/questions/20953390/what-is-the-fastest-hash-function-for-pointers
         */
        size_t h(const Vertex* key) const noexcept
        {
            /* NOTE: can't make constexpr; error handling sepc doesn't allow
             * <cmath> functions (e.g., std::log2) to be comile time constants
             */
            static const double pbits = std::log2(1 + sizeof(Vertex));
            static const size_t shift = static_cast<size_t>(pbits);

            return reinterpret_cast<size_t>(key) >> shift;
        }
    };

    class NeighborIterator
    {
      private:
        using _self = NeighborIterator;

      public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = Neighbor;
        using reference         = Neighbor&;
        using pointer           = Neighbor*;

        NeighborIterator() = default;
        NeighborIterator(Node* node) noexcept : node(node) {}
        NeighborIterator(const _self&) = default;

        NeighborIterator& operator=(const _self&) = default;

        reference operator*() const noexcept
        {
            return *static_cast<NeighborNode*>(node)->get();
        }

        pointer operator->() const noexcept
        {
            return static_cast<NeighborNode*>(node)->get();
        }

        _self& operator++() noexcept
        {
            node = node->next;
            return *this;
        }

        _self operator++(int) noexcept
        {
            _self tmp(*this);
            ++*this;
            return tmp;
        }

        _self& operator--() noexcept
        {
            node = node->prev;
            return *this;
        }

        _self operator--(int) noexcept
        {
            _self tmp(*this);
            --*this;
            return tmp;
        }

        friend bool operator==(const _self& lhs, const _self& rhs) noexcept
        {
            return lhs.node == rhs.node;
        }

        friend bool operator!=(const _self& lhs, const _self& rhs) noexcept
        {
            return lhs.node != rhs.node;
        }

      private:
        Node* node;
    };

    class ConstNeighborIterator
    {
      private:
        using _self = ConstNeighborIterator;

      public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = Neighbor;
        using reference         = const Neighbor&;
        using pointer           = const Neighbor*;

        ConstNeighborIterator() = default;
        ConstNeighborIterator(const Node* node) noexcept : node(node) {}

        ConstNeighborIterator(const _self&) = default;

        _self& operator=(const _self&) = default;

        reference operator*() const noexcept
        {
            return *static_cast<const NeighborNode*>(node)->get();
        }

        pointer operator->() const noexcept
        {
            return static_cast<const NeighborNode*>(node)->get();
        }

        _self& operator++() noexcept
        {
            node = node->next;
            return *this;
        }

        _self operator++(int) noexcept
        {
            _self tmp(*this);
            ++*this;
            return tmp;
        }

        _self& operator--() noexcept
        {
            node = node->prev;
            return *this;
        }

        _self operator--(int) noexcept
        {
            _self tmp(*this);
            --*this;
            return tmp;
        }

        friend bool operator==(const _self& lhs, const _self& rhs) noexcept
        {
            return lhs.node == rhs.node;
        }

        friend bool operator!=(const _self& lhs, const _self& rhs) noexcept
        {
            return lhs.node != rhs.node;
        }

      private:
        const Node* node;
    };

    struct Vertex : AdjacencyNode {
        Vertex() = default;
        Vertex(const T& data, AdjacencyNode* owner) noexcept
            : AdjacencyNode(owner), data(data)
        {
        }

        Vertex(const Vertex& source, AdjacencyNode* owner) noexcept
            : AdjacencyNode(owner), data(source.data)
        {
        }

        bool connect(Vertex* v, const CostPtr& cost) noexcept
        {
            return neighborhood.connect(v, cost);
        }

        bool disconnect(Vertex* v) noexcept
        {
            return neighborhood.disconnect(v);
        }

        Cost cost(Vertex* v) const { return neighborhood.cost(v); }

        size_t degree() const noexcept { return neighborhood.size(); }

        size_t isolate() noexcept
        {
            size_t deleted = degree();
            neighborhood.disassociate(this);
            neighborhood.clear();
            return deleted;
        }

        T data;
        Neighborhood neighborhood;

        inline T* get() noexcept { return std::addressof(data); }
        inline const T* get() const noexcept { return std::addressof(data); }
    };

    class VertexIterator
    {
        friend class ConstVertexIterator;
        friend class ::Neighborhood<VertexIterator>;

      private:
        using _self = VertexIterator;

        using Address = const Node;

        using Storage = typename Neighborhood::Storage;

        using neighbor_iterator      = NeighborIterator;
        using const_neighbor_iteator = ConstNeighborIterator;

        using neighbor_value_type      = Neighbor;
        using neighbor_reference       = Neighbor&;
        using neighbor_const_reference = const Neighbor&;

      public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = T;
        using pointer           = T*;
        using reference         = T&;

        VertexIterator() = default;
        VertexIterator(AdjacencyNode* node) noexcept : node(node) {}
        VertexIterator(const _self&) = default;

        _self& operator=(const _self&) = default;

        reference operator*() const noexcept
        {
            return *static_cast<Vertex*>(node)->get();
        }

        pointer operator->() noexcept
        {
            return static_cast<Vertex*>(node)->get();
        }

        _self& operator++() noexcept
        {
            node = node->next;
            return *this;
        }

        _self operator++(int) noexcept
        {
            _self tmp(*this);
            ++*this;
            return tmp;
        }

        _self& operator--() noexcept
        {
            node = node->prev;
            return *this;
        }

        _self operator--(int) noexcept
        {
            _self tmp(*this);
            --*this;
            return tmp;
        }

        friend bool operator==(const _self& rhs, const _self& lhs) noexcept
        {
            return lhs.node == rhs.node;
        }

        friend bool operator!=(const _self& rhs, const _self& lhs) noexcept
        {
            lhs.node != rhs.node;
        }

      private:
        AdjacencyNode* node = nullptr;

        Storage* neighborhood() noexcept
        {
            auto v = static_cast<Vertex*>(node);
            return v->neighborhood.storage;
        }
    };

    class ConstVertexIterator
    {
        friend class Graph;
        friend class ::Neighborhood<ConstVertexIterator>;

      private:
        using _self = ConstVertexIterator;

        using Address = const Node;

        using Storage = typename Neighborhood::Storage;

        using neighbor_iterator      = ConstNeighborIterator;
        using const_neighbor_iteator = ConstNeighborIterator;

        using neighbor_value_type      = Neighbor;
        using neighbor_reference       = const Neighbor&;
        using neighbor_const_reference = const Neighbor&;

      public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = T;
        using pointer           = const T*;
        using reference         = const T&;

        ConstVertexIterator() = default;
        ConstVertexIterator(const AdjacencyNode* node) noexcept : node(node) {}
        ConstVertexIterator(const _self&) = default;

        ConstVertexIterator(const VertexIterator& source) noexcept
            : node(source.node)
        {
        }

        _self& operator=(const _self&) = default;

        reference operator*() const noexcept
        {
            return *static_cast<const Vertex*>(node)->get();
        }

        pointer operator->() const noexcept
        {
            return static_cast<const Vertex*>(node)->get();
        }

        _self& operator++() noexcept
        {
            node = node->next;
            return *this;
        }

        _self operator++(int) noexcept
        {
            _self tmp(*this);
            ++*this;
            return tmp;
        }

        _self& operator--() noexcept
        {
            node = node->prev;
            return *this;
        }

        _self operator--(int) noexcept
        {
            _self tmp(*this);
            --*this;
            return tmp;
        }

        friend bool operator==(const _self& rhs, const _self& lhs) noexcept
        {
            return lhs.node == rhs.node;
        }

        friend bool operator!=(const _self& rhs, const _self& lhs) noexcept
        {
            return lhs.node != rhs.node;
        }

      private:
        const AdjacencyNode* node = nullptr;

        inline AdjacencyNode* extract() const noexcept
        {
            return const_cast<AdjacencyNode*>(node);
        }

        Storage* neighborhood() noexcept
        {
            auto v = static_cast<Vertex*>(const_cast<AdjacencyNode*>(node));
            return v->neighborhood.storage;
        }
    };

  public:
    /**
     * @brief Default constructor. Constructs an empty graph.
     */
    Graph() = default;

    /**
     * @brief Copy constructor. Constructs a graph with the same elements and
     * edges as the source.
     *
     * @param source The graph to copy from.
     */
    Graph(const Graph& source) : adj(new AdjacencyList{*source.adj}) {}

    /**
     * @brief Move constructor. Constructs a graph by moving the elements of the
     * source graph into the newly constructed graph.
     *
     * @param source The graph to move from.
     */
    Graph(Graph&& source) : adj(std::exchange(source.adj, new AdjacencyList{}))
    {
    }

    ~Graph() { delete adj; }

    Graph& operator=(const Graph& source)
    {
        if (&source != this) {
            delete adj;
            adj = new AdjacencyList{*source.adj};
        }

        return *this;
    }

    Graph& operator=(Graph&& source)
    {
        adj = std::exchange(source.adj, new AdjacencyList{});
        return *this;
    }

    /**
     * @brief Inserts an element into the graph.
     *
     * @param data The element to be inserted.
     * @return An iterator pointing to the inserted element.
     */
    iterator insert(const T& data) noexcept
    {
        return iterator{adj->insert(adj->prev, data)};
    }

    // TODO: add other insert methods (init list, hinted, etc...)

    /**
     * @brief Erases the specified element from the graph.
     *
     * @param v An iterator pointing to the element to be removed.
     * @return An iterator pointing to the element following the removed
     * element.
     */
    iterator erase(const_iterator v)
    {
        return iterator{adj->erase(v.extract())};
    }

    // TODO: add other erase methods

    /**
     * @brief Create an edge between two existing elements of the graph.
     *
     * @param u An iterator pointing to the first end of the edge.
     * @param v An iterator pointing to the other end of the edge.
     * @param cost The cost between u and v.
     */
    void collocate(const_iterator u, const_iterator v, const Cost& cost)
    {
        adj->collocate(u.extract(), v.extract(), cost);
    }

    /**
     * @brief Remove an edge that exists between two elements of the graph.
     *
     * @param u An iterator pointing to the first end of the edge.
     * @param v An iterator pointing to the other end of the edge.
     */
    void sever(const_iterator u, const_iterator v)
    {
        adj->sever(u.extract(), v.extract());
    }

    /**
     * @brief Get the cost of an existing edge in the graph.
     *
     * @param u An iterator pointing to the first end of the edge.
     * @param v An iterator pointing to the other end of the edge.
     * @return The cost of the edge.
     */
    Cost cost(const_iterator u, const_iterator v) const
    {
        return adj->cost(u.extract(), v.extract());
    }

    /**
     * @brief Get the number of other elements that a specified element is
     * realted to (by an edge). This number is refered to as the degree of the
     * element.
     *
     * @param v An iterator pointing to the element being queried.
     * @return The non-negative number indicating the degree of the element.
     */
    size_t degree(const_iterator v) const { return adj->degree(v.extract()); }

    /**
     * @brief Get the number of elements the graph contians.
     *
     * @return The non-negative number of elements in the graph.
     */
    size_t size() const noexcept { return adj->size; }

    /**
     * @brief Get the number of edges between elements of the graph.
     *
     * @return The non-negative number of edges in the graph.
     */
    size_t edges() const noexcept { return adj->edges; }

    /**
     * @brief Checks if the graph contains no elements.
     *
     * @return True if the graph is empty, and false otherwise
     */
    bool empty() const noexcept { return begin() == end(); }

    /**
     * @brief Combine two graphs together. Moves the elements of the argument
     * graph into the calling graph. Note that the argument graph will be empty
     * after a call to splice. Also this results in a disconnected graph.
     *
     * @param other The other graph to be combined with this graph.
     */
    void splice(Graph&& other) { adj->splice(std::move(*other.adj)); }

    /**
     * @brief Get an iterator to the first element in the graph.
     *
     * @return Iterator to the first element.
     */
    iterator begin() { return iterator{adj->begin()}; }

    /**
     * @brief Get an iterator to the first element in the graph.
     *
     * @return Iterator to the first element.
     */
    const_iterator begin() const { return cbegin(); }

    /**
     * @brief Get an iterator to the first element in the graph.
     *
     * @return Iterator to the first element.
     */
    const_iterator cbegin() const { return const_iterator{adj->begin()}; }

    /**
     * @brief Get an iterator representing the end of the elements in the graph.
     *
     * @return Iterator to the element following the last element.
     */
    iterator end() { return iterator{adj->end()}; }

    /**
     * @brief Get an iterator representing the end of the elements in the graph.
     *
     * @return Iterator to the element following the last element.
     */
    const_iterator end() const { return cend(); }

    /**
     * @brief Get an iterator representing the end of the elements in the graph.
     *
     * @return Iterator to the element following the last element.
     */
    const_iterator cend() const { return const_iterator{adj->end()}; }

  private:
    AdjacencyList* adj = new AdjacencyList{};
};

/**
 * @brief Companion class for graph. Represents all the adjacent elements of a
 * given element of a graph. This class provides an interface to iterate over
 * these elements. This class also contains logic for marking specific elements
 * as "visited" (i.e., already iterated over when considered adjacent to some
 * other element). Some elements may be adjacent to multiple other elements and
 * there are instances where iterating over the same element more than
 * once is undesireable.
 *
 * @tparam Iterator An iterator (const or not) pointing to an element of a
 * graph.
 */
template <typename Iterator> class Neighborhood
{
  private:
    using Address = typename Iterator::Address;
    using Storage = typename Iterator::Storage;

    struct Entry {
        Entry() = default;
        Entry(const size_t address) noexcept : address(address) {}

        Entry(const size_t address, const bool seen) noexcept
            : address(address), seen(seen)
        {
        }

        friend bool operator==(const Entry& lhs, const Entry& rhs) noexcept
        {
            return lhs.address == rhs.address;
        }

        friend bool operator!=(const Entry& lhs, const Entry& rhs) noexcept
        {
            return lhs.address != rhs.address;
        }

        size_t address;
        bool seen = false;
    };

    struct Directory {
        Directory() = default;

        Directory(const Directory&) = delete;

        Directory(Directory&& source) noexcept
            : directory(std::move(source.directory)), next(source.next)
        {
        }

        Directory& operator=(const Directory&) = delete;

        Directory& operator=(Directory&& source) noexcept
        {
            directory = std::move(source.directory);
            next      = source.next;
            return *this;
        }

        size_t& search(Address* addr) noexcept
        {
            try {
                return directory.at(addr).address;
            } catch (const std::out_of_range& e) {
                return (directory[addr] = Entry{next++}).address;
            }
        }

        bool seen(Address* addr) noexcept
        {
            try {
                return directory.at(addr).seen;
            } catch (const std::out_of_range& e) {
                return (directory[addr] = Entry{next++}).seen;
            }
        }

        void checkoff(Address* addr)
        {
            try {
                directory.at(addr).seen = true;
            } catch (const std::out_of_range& e) {
                directory[addr] = Entry{next++, true};
            }
        }

        std::unordered_map<Address*, Entry> directory;
        size_t next = 0;
    };

  public:
    using value_type      = typename Iterator::neighbor_value_type;
    using reference       = typename Iterator::neighbor_reference;
    using const_reference = typename Iterator::neighbor_const_reference;

    using iterator       = typename Iterator::neighbor_iterator;
    using const_iterator = typename Iterator::const_neighbor_iteator;

    Neighborhood() = default;

    Neighborhood(Iterator it) noexcept : storage(it.neighborhood()) {}

    Neighborhood(Iterator it, Neighborhood&& other) noexcept
        : storage(it.neighborhood()), directory(std::move(other.directory))
    {
    }

    Neighborhood(const Neighborhood&) = delete;

    Neighborhood(Neighborhood&& source) noexcept
        : storage(source.storage), directory(std::move(source.directory))
    {
    }

    Neighborhood& operator=(const Neighborhood&) = delete;

    Neighborhood& operator=(Neighborhood&& source) noexcept
    {
        storage   = source.storage;
        directory = std::move(source.directory);
        return *this;
    }

    size_t search(Iterator it) noexcept { return directory.search(it.node); }
    bool seen(Iterator it) noexcept { return directory.seen(it.node); }
    void checkoff(Iterator it) { directory.checkoff(it.node); }

    iterator begin() noexcept { return iterator{storage->next}; }
    const_iterator begin() const noexcept { return cbegin(); }

    const_iterator cbegin() const noexcept
    {
        return const_iterator{storage->next};
    }

    iterator end() noexcept { return iterator{storage}; }
    const_iterator end() const noexcept { return cend(); }
    const_iterator cend() const noexcept { return const_iterator{storage}; }

  private:
    Storage* storage = nullptr;
    Directory directory;
};

template <typename Iterator, typename... Args>
Neighborhood<Iterator> neighborhood(Iterator it, Args&&... args)
{
    return Neighborhood<Iterator>{it, std::forward<Args>(args)...};
}

#endif
